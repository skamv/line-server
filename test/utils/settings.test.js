const assert = require('assert');
const Settings = require("../../src/utils/settings");

const firstSettings = { firstKey: "firstValue" };
const additionalSettings = { secondKey: "secondValue" };
const updatedSettings = { firstKey: "secondValue" };


describe('Settings', () => {
    beforeEach(() => Settings.reset());

    it('should be empty', () => {
        assert.deepStrictEqual(Settings.get(), {});
    });

    it('should merge', () => {
        Settings.merge(firstSettings);
        assert.deepStrictEqual(Settings.get(), firstSettings);

        Settings.merge(additionalSettings);
        assert.deepStrictEqual(Settings.get(), { ...firstSettings, ...additionalSettings });
    });

    it('should override', () => {
        Settings.merge(firstSettings);
        assert.deepStrictEqual(Settings.get(), firstSettings);

        Settings.merge(updatedSettings);
        assert.deepStrictEqual(Settings.get(), updatedSettings);
    });

    it('should reset', () => {
        Settings.merge(firstSettings);
        assert.deepStrictEqual(Settings.get(), firstSettings);

        Settings.reset();
        assert.deepStrictEqual(Settings.get(), {});
    });
});
