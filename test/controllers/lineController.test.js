const assert = require('assert');
const { teardown } = require("mocha");
const mock = require('mock-fs');
const Server = require("../../src/server");
const FileIndexer = require("../../src/tools/fileIndexer");
const Settings = require("../../src/utils/settings");

const MOCK_DIR = 'mock/path';
const MOCK_FILE = 'test-file.txt';

// INFO, skipping because I was having some trouble with the autoload of routes with mocha
describe.skip('Line Controller', () => {
    before(async () => {  
        mock({
            [MOCK_DIR]: {
                [MOCK_FILE]: 'a\nbb\nccc\n\neeeee\n',
            },
        });
        const filePath = `${MOCK_DIR}/${MOCK_FILE}`;
        const indexResponse = await FileIndexer.index({ filePath });
        Settings.merge({
            filePath,
            ...indexResponse,
        });
        Server.start();
    })
    
    it('should return 200', async () => {
        const instance = Server.getInstance();
        const response = await instance.inject({
            method: 'GET',
            url: '/lines/1'
        });
        assert.strictEqual(response.statusCode, 200);
    });

    teardown(() => instance.close());
});
