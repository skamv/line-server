const assert = require('assert');
const LineServices = require("../../src/services/lineServices");
const FileIndexer = require("../../src/tools/fileIndexer");

const mock = require('mock-fs');

const MOCK_DIR = 'mock/path';
const MOCK_FILE = 'test-file.txt';



describe('Line Services', () => {
    beforeEach(() => {
        mock({
            [MOCK_DIR]: {
                [MOCK_FILE]: 'a\nbb\nccc\n\neeeee\n',
            },
        });
    });

    it('should read lines', async () => {
        const filePath = `${MOCK_DIR}/${MOCK_FILE}`;
        const response = await FileIndexer.index({ filePath });
        let line;
        line = await LineServices.getLine({ lineID: 1, filePath, ...response });
        assert.strictEqual(line, "a");
        line = await LineServices.getLine({ lineID: 4, filePath, ...response });
        assert.strictEqual(line, "");
    });
});
