const assert = require('assert');
const FileIndexer = require("../../src/tools/fileIndexer");

const mock = require('mock-fs');

const MOCK_DIR = 'mock/path';
const MOCK_FILE = 'test-file.txt';


describe('FileIndexer', () => {
    beforeEach(() => {
        mock({
            [MOCK_DIR]: {
                [MOCK_FILE]: 'a\nbb\nccc\n\neeeee\n',
            },
        });
    });
    
    it('should process file', async () => {
        const filePath = `${MOCK_DIR}/${MOCK_FILE}`;
        const response = await FileIndexer.index({ filePath });
        assert.strictEqual(response.indexLength, 2);
        assert.strictEqual(response.totalLines, 5);
        assert.strictEqual(response.indexesFilePath, `${filePath}_index`);
    });

    it('should throw ENOENT', async () => {
        const filePath = `nonexistent`;
        try {
            await FileIndexer.index({ filePath });
            assert.rejects();
        } catch (error) {
            assert.strictEqual(error.code, "ENOENT")
        }
    });
});
