import http from 'k6/http';
import { sleep } from 'k6';

export let options = {
    stages: [
        { duration: '10s', target: 100 },
        { duration: '1m', target: 10000 },
        { duration: '10s', target: 100 },
    ],
    discardResponseBodies: true,
};

export default () => {
    http.get(`http://localhost:3000/lines/${Math.floor(Math.random() * 10) + 1  }`);
    sleep(1);
};