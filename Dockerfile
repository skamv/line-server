FROM node:14-alpine

WORKDIR /usr/src/server

COPY package*.json ./

RUN npm ci --only=production

COPY src/ ./

EXPOSE 3000

CMD [ "node", "index.js", "/usr/file"]
