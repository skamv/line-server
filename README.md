# Line Server

A system that is capable of serving lines out of a file to network clients.

Serves individual lines of an immutable text file over the network to clients using the following simple REST API:

```text
GET /lines/<line index>
```

Returns an HTTP status of `200` and the text of the requested line or an HTTP `413` status if the requested line is beyond the end of the file.

![Interaction Example](./assets/interaction_example.gif)

---

## Requirements

- [node](https://nodejs.org/en/blog/release/v14.15.0/) v14.15.0
- [npm](https://www.npmjs.com/package/npm/v/6.14.8) v6.14.8

[nvm](https://github.com/nvm-sh/nvm) is recommended for setting up your node.js environment.

## External Dependencies

Node packages:

- [fastify](https://www.npmjs.com/package/fastify) ^3.7.0
- [fastify-autoroutes](https://www.npmjs.com/package/fastify-autoroutes) ^1.0.23
- [mocha](https://www.npmjs.com/package/mocha) ^8.2.0
- [mock-fs](https://www.npmjs.com/package/mock-fs) ^4.13.0

## Usage

![Usage Example](./assets/usage_example.gif)

**Step 1**: Run the build script:

```sh
./build.sh
```

**Step 2**: Execute the run script by passing a file path as an argument:

```sh
./run.sh /path/to/file
```

## Docker Usage

This app can run as a Docker container:

**Step 1**: Build the Docker image:

```sh
docker build -t line-server .
```

**Step 2**: Run the Docker container locally (notice that you must pass an absolute path for the host file to be served):

```sh
docker run -p 8080:3000 \
    -v /absolute/path/to/host/file:/usr/file \
    -d line-server:latest
```

---

## Questions

### How does your system work?

When the system is started, the pre-processing phase takes place. Once it is finished, the HTTP server is started right after automatically.

#### **Pre-processing Phase**

1. We start by calculating the total file size in bytes (this is useful later on for adding leading zeros to the offset).
2. The file provided to the system is read using a `read stream`.
Like that, we make sure that the file is never loaded entirely into memory.
3. We also create a `write stream` for the indexation file (e.g. 'immutable_filename.txt_index').
4. We stream the immutable file line by line.
5. For each line, we get the starting byte offset and increment a total lines counter (useful for validations later on).
6. The byte offset is padded with leading zeros till it reaches the length of the total file size in bytes (e.g. total file size in bytes is 3204903, the length is 7 so a byte offset `5` is transformed to `0000005`).
We do this so that we can navigate in the index file with some basic calculations for a determined line number, thus reading lines from it in O(1).
7. We store some global data to be used by the HTTP server: immutable file path, index file path, totalLines, and indexLength

#### **Server Phase**

1. An HTTP server is made available on port 3000.
2. A `GET /lines/:id` route is registered.
3. This route validates that the `:id` parameter is a number (through schema validation provided by the web framework used).
4. After receiving a request we check if the line requested is between 1 and the number of total lines. If it does not belong to this range, we reply with HTTP status `413`.
5. For a valid line number, we compute the position of the byte offset information in the index file.
We read the index file from `startOffset = (lineID - 1) * indexLength` till `endOffset = startOffset + (indexLength - 1)` using a `read stream` (once again we just load what we need into memory). This operation is O(1).
6. At this point we have the information about the start byte of the line in the immutable file.
7. We use a `read stream` to start reading the immutable file from the computed start byte offset. We read it till we find a newline character `\n`. We retrieved the line. This operation is once again O(1).
8. we reply with HTTP status `200`, Content-Type header `text; charset=ascii` and the body contains the line text.

### How will your system perform with a 1 GB file? a 10 GB file? a 100 GB file?

I was able to test only with files till ~800MB (~6 seconds for pre-processing).
The amount of time spent on the pre-processing phase will scale linearly with the size of the file.

Regarding the Server Phase, I strongly believe that the system will behave in a similar way no matter the file size or index file size.

After some empirical observations, I concluded as well that the RAM ranged between 55MB and 75MB both during the pre-processing or while I executed request loading tests on the server.
The true limiting factor is the speed of the IO operations (depends on the host hardware).

The size of the index file that is built by the system will be greater, mostly because of the leading zeros padding added to the byte offsets together with the total amount of lines in the immutable file.

### How will your system perform with 100 users? 10000 users? 1000000 users?

I will assume that by users we mean concurrent connections.

I actually started by doing some experiments with [k6](https://k6.io/docs/test-types/load-testing), however, I have run into some local limitations (memory leaks using k6 with connections above 1k) and had to switch to [autocannon](https://github.com/mcollina/autocannon).

The demo file `demo/10lines.txt` was used for the following tests

- Testing with **100 connections** (users) during **60 seconds**, the system managed to process **11505.92 Req/Sec**

```bash
$ npx autocannon -d 60 -c 100 http://localhost:3000/lines/5
npx: installed 42 in 2.73s
Running 60s test @ http://localhost:3000/lines/5
100 connections

┌─────────┬──────┬──────┬───────┬──────┬─────────┬─────────┬───────┐
│ Stat    │ 2.5% │ 50%  │ 97.5% │ 99%  │ Avg     │ Stdev   │ Max   │
├─────────┼──────┼──────┼───────┼──────┼─────────┼─────────┼───────┤
│ Latency │ 6 ms │ 6 ms │ 8 ms  │ 9 ms │ 6.39 ms │ 1.06 ms │ 72 ms │
└─────────┴──────┴──────┴───────┴──────┴─────────┴─────────┴───────┘
┌───────────┬─────────┬─────────┬─────────┬─────────┬─────────┬────────┬─────────┐
│ Stat      │ 1%      │ 2.5%    │ 50%     │ 97.5%   │ Avg     │ Stdev  │ Min     │
├───────────┼─────────┼─────────┼─────────┼─────────┼─────────┼────────┼─────────┤
│ Req/Sec   │ 10103   │ 12415   │ 14935   │ 15503   │ 14579.6 │ 920.37 │ 10100   │
├───────────┼─────────┼─────────┼─────────┼─────────┼─────────┼────────┼─────────┤
│ Bytes/Sec │ 1.92 MB │ 2.36 MB │ 2.84 MB │ 2.95 MB │ 2.77 MB │ 175 kB │ 1.92 MB │
└───────────┴─────────┴─────────┴─────────┴─────────┴─────────┴────────┴─────────┘

Req/Bytes counts sampled once per second.

875k requests in 60.03s, 166 MB read
```

- Testing with **10000 connections** (users) during **60 seconds**, the system managed to process **11505.92 Req/Sec**.

```bash
$ npx autocannon -d 60 -c 10000 http://localhost:3000/lines/5
npx: installed 42 in 1.919s
Running 60s test @ http://localhost:3000/lines/5
10000 connections

┌─────────┬────────┬────────┬────────┬─────────┬────────┬───────────┬─────────┐
│ Stat    │ 2.5%   │ 50%    │ 97.5%  │ 99%     │ Avg    │ Stdev     │ Max     │
├─────────┼────────┼────────┼────────┼─────────┼────────┼───────────┼─────────┤
│ Latency │ 779 ms │ 851 ms │ 984 ms │ 1712 ms │ 866 ms │ 124.66 ms │ 3041 ms │
└─────────┴────────┴────────┴────────┴─────────┴────────┴───────────┴─────────┘
┌───────────┬────────┬─────────┬─────────┬─────────┬──────────┬─────────┬────────┐
│ Stat      │ 1%     │ 2.5%    │ 50%     │ 97.5%   │ Avg      │ Stdev   │ Min    │
├───────────┼────────┼─────────┼─────────┼─────────┼──────────┼─────────┼────────┤
│ Req/Sec   │ 527    │ 9711    │ 10927   │ 15511   │ 11505.92 │ 2148.91 │ 527    │
├───────────┼────────┼─────────┼─────────┼─────────┼──────────┼─────────┼────────┤
│ Bytes/Sec │ 100 kB │ 1.84 MB │ 2.08 MB │ 2.95 MB │ 2.19 MB  │ 408 kB  │ 100 kB │
└───────────┴────────┴─────────┴─────────┴─────────┴──────────┴─────────┴────────┘

Req/Bytes counts sampled once per second.

690k requests in 60.97s, 131 MB read
```

I was not able to create 1000000 concurrent connections since my local environment is acting both as a server and client(S). However, from the data previously shown for lower amounts of concurrent requests, I believe that the system is mainly limited by the hardware available on the host machine as well as on hard limit configuration (e.g. maximum file descriptors open). 

### What documentation, websites, papers, etc did you consult in doing this assignment?

- Nodejs File System: https://nodejs.org/api/fs.html
- Nodejs Stream: https://nodejs.org/api/stream.html
- Nodejs Readline: https://nodejs.org/api/readline.html
- Mocha test framework: https://mochajs.org
- Fastify web framework: https://www.fastify.io/docs/latest
- Load testing with autocannon: https://github.com/mcollina/autocannon
- Load testing with k6: https://k6.io/docs/test-types/load-testing)

**What third-party libraries or other tools does the system use? How did you choose each library or framework you used?**

Every decision was made after some research, by reading articles with pros and cons and weighing it together with community activity and documentation quality.

Web framework:

My first impulse was to use [Hapi](https://hapi.dev/) (due to previous work experience).
However, I ended up searching for alternatives, trying to find some benchmarking comparisons.
I ended up finding [some](https://github.com/fastify/benchmarks) and I concluded that Fastify was the one with the least overhead, allowing for more requests throughput.
Then, I had to be sure it would provide me the features I needed, so I spend some time reading the documentation, checking the source code to see if it has an active community. It got my green light for those aspects.

Testing framework:

I decided to go with [Mocha](https://mochajs.org) mainly because it allows some flexibility and customization, it has no built-in assertion library, and handling asynchronous code seemed pretty straightforward.


**How long did you spend on this exercise? If you had unlimited more time to spend on this, how would you spend it and how would you prioritize each item?**

Overall: 15 hours (over 5 days)

- Solution Design: 6 hours
- Load testing: 2 hours
- Implementation: 5 hours
- Manual Testing and automated tests: 2 hours

I would invest my unlimited time in the following:

- Horizontal scaling;
- Use distributed, in-memory key–value database (e.g. [Redis](https://redis.io/));
- Add [Cache control](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Cache-Control);
- Add Cluster mode for the nodejs app;
- Application metrics (e.g. [Prometheus](https://prometheus.io/)) and setup dashboards for it (e.g. [Grafana](https://github.com/grafana/grafana));
- Logging;
- Build and deployment pipelines;

**If you were to critique your code, what would you have to say about it?**

I am not using any configuration file to customize the application behavior (i.e. server port, index files destination), and this is leading me to use some hardcoded strings (e.g. 'ascii', '_index').

I also don't like the fact that my REST API returns directly the line content ('text') instead of an object ('application/json'), but that might have been my too strict interpretation of 'Returns an HTTP status of 200 and the text of the requested line'

---

## Local System Information

```bash
memory         16GiB System memory
processor      Intel(R) Core(TM) i7-8750H CPU @ 2.20GHz
os             Ubuntu x86_64 5.4.0-52-generic GNU/Linux
```
