
const fs = require("fs");
const readline = require("readline");

const getLine = ({ lineID, filePath, indexesFilePath, indexLength }) => new Promise((resolve, reject) => {
    const startOffset = (lineID - 1) * indexLength;
    const endOffset = startOffset + (indexLength - 1);
    const indexReadStream = fs.createReadStream(indexesFilePath, {
        encoding: "ascii",
        start: startOffset,
        end: endOffset
    });
    indexReadStream.once("data", (startIndex) => {
        const readStream = fs.createReadStream(filePath, { encoding: "ascii", start: Number(startIndex) });
        const readInterface = readline.createInterface({
            input: readStream,
        });
        readInterface.once("line", (line) => {
            readInterface.close();
            readStream.close();
            resolve(line);
        });
        indexReadStream.close();
    });
    indexReadStream.on("error", (error) => reject(error));
});


module.exports = {
    getLine
}
