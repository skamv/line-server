const Settings = require("../utils/settings");
const LineServices = require("../services/lineServices");

const readLineController = async (request, reply) => {
    const lineID = request.params.id;
    const settings = Settings.get();

    if (lineID < 1 || lineID > settings.totalLines) {
        return reply.code(413).send();
    };

    const line = await LineServices.getLine({
        lineID,
        filePath: settings.filePath,
        indexesFilePath: settings.indexesFilePath,
        indexLength: settings.indexLength,
    })

    return reply
        .code(200)
        .header("Content-Type", "text; charset=ascii")
        .send(line)
}

module.exports = {
    readLineController
}
