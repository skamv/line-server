const fs = require("fs");
const readline = require("readline");

const addPadding = ({ value, length, padContent = "0" }) =>
    value.toString().padStart(length, padContent)

const getFileSizeInBytes = ({ path }) => fs.statSync(path)["size"];

const index = ({ filePath }) => new Promise((resolve, reject) => {
    const indexesFilePath = filePath + "_index";
    const fileSizeInBytes = getFileSizeInBytes({ path: filePath });
    const indexLength = fileSizeInBytes.toString().length;
    
    const readStream = fs.createReadStream(filePath, {
        encoding: "ascii"
    });
    const writeStream = fs.createWriteStream(indexesFilePath, {
        encoding: "ascii" 
    });
    const readLineStream = readline.createInterface({
        input: readStream
    });

    let byte = 0;
    let totalLines = 0;
    readLineStream.on("line", (line) => {
        const paddedByteOffset = addPadding({ value: byte, length: indexLength });
        writeStream.write(paddedByteOffset);
        byte += Buffer.byteLength(line + "\n", "ascii");
        totalLines++;
    });

    readLineStream.on("close", () => {
        writeStream.close();
        resolve({
            indexesFilePath,
            totalLines,
            indexLength,
        })
    });

    readStream.on("error", (error) => reject(error));
    writeStream.on("error", (error) => reject(error));
})

const FileIndexer = module.exports = {
    index
}
