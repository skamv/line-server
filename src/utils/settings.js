let data = {};

const Settings = module.exports = {
    merge: (settings) => data = { ...data, ...settings },
    get: () => data,
    reset: () => { data = {} }
}
