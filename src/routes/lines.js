const { readLineController } = require("../controllers/lineControllers")

module.exports = async (fastify) => {
    fastify.route({
        method: "GET",
        url: "/lines/:id",
        schema: {
            params: {
                type: "object",
                required: [ "id" ],
                properties: {
                    id: {
                        type: "number"
                    } 
                }
            }
        },
        handler: readLineController
    })
}