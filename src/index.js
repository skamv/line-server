const Server = require("./server");
const Settings = require("./utils/settings");
const FileIndexer = require("./tools/fileIndexer");

const main = async () => {
    const filePath = process.argv[2];
    try {
        if (!filePath) {
            throw new Error('File path missing');
        }
        console.log("Indexing process started:", filePath);
        console.time("Indexing time", filePath);
        const { 
            indexesFilePath,
            totalLines,
            indexLength
        } = await FileIndexer.index({ filePath });
        Settings.merge({
            filePath,
            indexesFilePath,
            totalLines,
            indexLength
        });
        console.log("Settings: ", JSON.stringify(Settings.get(), null, 4));
        console.timeEnd("Indexing time");
        console.log("Starting server...");
        Server.start();
    } catch (error) {
        console.log('ERROR:', error.message);
        process.exit(0);
    }
}

main();
