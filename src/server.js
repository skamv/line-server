const Fastify = require("fastify");
const AutoRoutes = require("fastify-autoroutes");
const path = require("path");

const PORT = 3000;

let instance;

const start = () => {
    instance = Fastify();

    instance.register(AutoRoutes, {
        dir: path.join(__dirname, "routes"),
    });

    instance.listen(PORT, '0.0.0.0', (error) => {
        if (error) {
            console.log(error);
            instance.close();
            process.exit(1);
        }
        console.log("Server started successfully. Listening on port:", PORT);
    });
}

const getInstance = () => instance;

module.exports = {
    start,
    getInstance,
}
